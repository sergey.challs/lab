public class Int {
    private int value;
    
    public Int() {
        value = 0;
    }

    public Int(int value) {
        this.value = value;
    }

    public Int increment() {
        ++value;
        return this;
    }
    
    public Int decrement() {
        --value;
        return this;
    }
    
    public Int add(Int p) {
        if (p.getValue() == 0) {
            return this;
        }
        else if (p.getValue()<0) {
            decrement();
            return add(p.increment());
        }
        else {
            increment();
            return add(p.decrement());
        }
    }

    public Int substract(Int p) {
        if (p.getValue() == 0) {
            return this;
        }
        else if (p.getValue() < 0) {
            increment();
            return substract(p.increment());
        }
        else {
            decrement();
            return substract(p.decrement());
        }
    }

    public String toString() {
        return "" + getValue();
    }
    
    public int getValue() {
        return value;
    }
    
    public static void main(String args[]) {
        Int a = new Int();
        a.increment();
        a.increment();
        while (a.getValue() < 1000) {
            Int b = new Int(a.getValue());
            a.add(b);
            System.out.println("" + a.toString());
        }
        while (a.getValue() != 1000) {
            a.decrement();
            System.out.println("" + a.toString());
        }
        while (a.getValue() != 0){
            Int b = new Int(a.getValue());
            a.substract(b);
        }
        System.out.println("" + a.toString());
    }
}
